from rest_framework import serializers, validators
from note.models import Note


class NoteSerializer(serializers.Serializer):
  unique_title_validator = validators.UniqueValidator(
    queryset=Note.objects.all(), message='title already exists!')
  id = serializers.IntegerField(read_only=True)
  title = serializers.CharField(required=True, allow_blank=False, max_length=100,
  validators=[unique_title_validator])
  text = serializers.CharField(required=False, allow_blank=True, max_length=500)
  url = serializers.HyperlinkedIdentityField(view_name='note_detail', lookup_field='id', read_only=True)

  def create(self, validated_data):
    """Create a Note from validated data."""
    return Note.objects.create(**validated_data)

  def update(self, instance, validated_data):
    """Update existing Note."""
    instance.title = validated_data.get('title', instance.title)
    instance.text = validated_data.get('text', instance.text)
    instance.save()
    return instance