from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from note import models
from note.serializers import NoteSerializer
from django.shortcuts import get_object_or_404


class NotesList(APIView):
  """Notes."""

  def get(self, request):
    notes = models.Note.objects.all()
    serializer_context = {'request': request}
    serializer = NoteSerializer(notes, many=True, context=serializer_context)
    return Response(data=serializer.data)

  def post(self, request):
    serializer_context = {'request': request}
    serializer = NoteSerializer(data=request.data, context=serializer_context)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NoteDetail(APIView):
  """Note by private key."""

  def get_not_by_id(self, id):
    return get_object_or_404(models.Note, pk=id)

  def get(self, request, id):
    note = self.get_not_by_id(id)
    serializer_context = {'request': request}
    serializer = NoteSerializer(note, context=serializer_context)
    return Response(serializer.data)

  def put(self, request, id):
    note = self.get_not_by_id(id)
    serializer_context = {'request': request}
    serializer = NoteSerializer(note, data=request.data, context=serializer_context)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
 
  def delete(self, request, id):
    note = self.get_not_by_id(id)
    note.delete()
    return Response({}, status=status.HTTP_204_NO_CONTENT)

