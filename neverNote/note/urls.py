from django.conf.urls import url
from note import views

urlpatterns = [
  url(r'^notes/$', views.NotesList.as_view(), name='notes_list'),
  url(r'^notes/(?P<id>\d+)/$', views.NoteDetail.as_view(), name='note_detail')
]