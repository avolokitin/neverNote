from django.db import models

class Note(models.Model):
  title = models.CharField(max_length=100, blank=False, null=False, unique=True)
  text = models.TextField(max_length=500, blank=True, default='')
  created = models.DateField(auto_now_add=True)

  class Meta:
    ordering = ('title',)

  def __str__(self):
    return self.title
