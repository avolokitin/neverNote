## Setup env:

#### dev-tools:
* [markdown syntax](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [virtualenv](https://virtualenv.pypa.io/en/stable/userguide/#usage)
* [tools review](https://stackoverflow.com/questions/41573587/what-is-the-difference-between-venv-pyvenv-pyenv-virtualenv-virtualenvwrappe)
* [pytest for testing] (https://docs.pytest.org/en/latest/)


1. pip install virtualenvwrapper
2. virtualenv ENV
3. source ENV/bin/activate
4. ENV/bin/pip install -r requirements.txt
5. echo ".gitignore" >> .gitignore
6. echo "ENV" >> .gitignore
7. echo "\_\_pycache\_\_" >> .gitignore
8. ENV/bin/pip install -r test-requirements.txt
9. pytest

#### for mac owners tools for consideration:
 - [iTerm2](https://www.iterm2.com/index.html)
 - zsh via brew
 - [Oh my zsh plugin collection](http://ohmyz.sh)
 - ptpython via brew
