FROM python:3.5.4-slim

COPY ./neverNote /app/neverNote
COPY ./requirements.txt /app
WORKDIR /app/neverNote
RUN pip install -r ../requirements.txt
EXPOSE 8000

ENTRYPOINT ["python"]
CMD [ "python", "./manage.py runserver" ]
